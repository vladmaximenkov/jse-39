package ru.vmaksimenkov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO `app_task` (`name`, `description`, `dateStart`, `dateFinish`, `userId`, `id`, `projectId`, `status`) " +
            "VALUES (#{task.name}, #{task.description}, #{task.dateStart}, #{task.dateFinish}, #{task.userId}, #{task.id}, #{task.projectId}, #{task.status})")
    void add(@Nullable @Param("task") Task task);

    @Insert({
            "<script>",
                "INSERT INTO `app_task` ",
                "(`name`, `description`, `dateStart`, `dateFinish`, `userId`, `id`, `projectId`, `status`) ",
                "VALUES" +
                "<foreach item='e' collection='taskList' open='' separator=',' close=''>" +
                    "(" +
                        "#{e.name},",
                        "#{e.description},",
                        "#{e.dateStart},",
                        "#{e.dateFinish},",
                        "#{e.userId},",
                        "#{e.id},",
                        "#{e.projectId},",
                        "#{e.status}" +
                    ")" +
                "</foreach>",
            "</script>"})
    void addAll(@Nullable @Param("taskList") List<Task> taskList);

    @Update("UPDATE `app_task` SET `projectId` = #{projectId} WHERE `userId` = #{userId} AND `taskId` = #{taskId}")
    void bindTaskPyProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId, @NotNull @Param("taskId") String taskId);

    @Delete("DELETE FROM `app_task` WHERE `userId` = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM `app_task`")
    void clearAll();

    @Select("SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END " +
            "FROM `app_task` WHERE `userId` = #{userId} AND `id` = #{id} LIMIT 1")
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END " +
            "FROM `app_task` WHERE `userId` = #{userId} AND `name` = #{name} LIMIT 1")
    boolean existsByName(@NotNull @Param("userId") String userId, @Nullable @Param("name") String name);

    @Select("SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END " +
            "FROM `app_task` WHERE `userId` = #{userId} AND `projectId` = #{projectId} LIMIT 1")
    boolean existsByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    //TODO check comparator
    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId` = #{userId} ORDER BY &{comparator.class}")
    List<Task> findAllSort(@NotNull @Param("userId") String userId, @NotNull @Param("comparator") Comparator<Task> comparator);

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId` = #{userId}")
    List<Task> findAllOfUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM `app_task`")
    List<Task> findAll();

    @Nullable
    @Select("SELECT * FROM `app_task` WHERE `userId` = #{userId} AND `projectId` = #{projectId}")
    List<Task> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Nullable
    @Select("SELECT * FROM `app_task` WHERE `id` = #{id}")
    Task findByIdAll(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `app_task` WHERE `id` = #{id} AND `userId` = #{userId}")
    Task findById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `app_task` WHERE `userId` = #{userId} LIMIT 1 OFFSET #{index}")
    Task findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT * FROM `app_task` WHERE `userId` = #{userId} AND `name` = #{name} LIMIT 1")
    Task findOneByName(@NotNull @Param("userId") String userId, @Nullable @Param("name") String name);

    @Nullable
    @Select("SELECT `id` FROM `app_task` WHERE `userId` = #{userId} LIMIT 1 OFFSET #{index}")
    String getIdByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Delete("DELETE FROM `app_task` WHERE `id` = #{task.id}")
    void remove(@Nullable @Param("task") Task task);

    @Delete("DELETE FROM `app_task` WHERE `userId` = #{userId} AND `projectId` IS NOT NULL")
    void removeAllBinded(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM `app_task` WHERE `userId` = #{userId} AND `projectId` = #{projectId}")
    void removeAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Delete("DELETE FROM `app_task` WHERE `id` = #{id} AND `userId` = #{userId}")
    void removeById(@NotNull @Param("userId") String userId, @Nullable @Param("id") String id);

    @Delete("DELETE FROM `app_task` WHERE `id` IN (SELECT `id` FROM (SELECT `id` FROM `app_task` WHERE `userId` = #{userId} LIMIT #{index}, 1) x)")
    void removeOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Delete("DELETE FROM `app_task` WHERE `userId` = #{userId} AND `name` = #{name}")
    void removeOneByName(@NotNull @Param("userId") String userId, @Nullable @Param("name") String name);

    @Select("SELECT COUNT(*) FROM `app_task`")
    int sizeAll();

    @Select("SELECT COUNT(*) FROM `app_task` WHERE `userId` = #{userId}")
    int size(@NotNull @Param("userId") String userId);

    @Update("UPDATE `app_task` SET `projectId` = NULL WHERE `userId` = #{userId} AND `id` = #{id}")
    void unbindTaskFromProject(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Update("UPDATE `app_task` SET `name` = #{task.name}, `description` = #{task.description}, `projectId` = #{task.projectId}" +
            "`dateStart` = #{task.dateStart}, `dateFinish` = #{task.dateFinish}, `userId` = #{task.userId}, " +
            "`status` = #{task.status} WHERE `id` = #{task.id}")
    void update(@Nullable @Param("task") Task task);

}

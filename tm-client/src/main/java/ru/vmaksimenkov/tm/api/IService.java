package ru.vmaksimenkov.tm.api;

import ru.vmaksimenkov.tm.endpoint.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}

package ru.vmaksimenkov.tm.api.entity;

import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.endpoint.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}

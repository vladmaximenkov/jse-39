package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;
import ru.vmaksimenkov.tm.exception.user.AlreadyLoggedInException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

public final class UserLoginCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Log in to the system";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-login";
    }

    @Override
    public void execute() {
        if (endpointLocator.getSession() != null) throw new AlreadyLoggedInException();
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = nextLine();
        if (!endpointLocator.getUserEndpoint().existsUserByLogin(login)) throw new UserNotFoundException();
        System.out.println("ENTER PASSWORD:");
        endpointLocator.setSession(
                endpointLocator.getSessionEndpoint().openSession(login, TerminalUtil.nextLine())
        );
    }

}
